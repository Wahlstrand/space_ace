#pragma once

#include <gmock/gmock.h>
#include "ecs/iecs_listener.h"

namespace ecs::test {
class MockECSListener : public IECSListener {
 public:
  MOCK_METHOD1(notifyChanged, void(EntityId id));
};
}  // namespace ecs::test