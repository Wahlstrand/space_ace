#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D lighting;
uniform sampler2D bloom;

void main()
{ 
    const float bloomIntensity = 1.0; //Example uses 1.0 here but that looks odd near the bloom cut-off
    vec3 hdrColor = texture(lighting, TexCoords).rgb +
                    bloomIntensity * texture(bloom, TexCoords).rgb;

    // tone mapping
    const float exposure = 1.0;
    vec3 result = vec3(1.0) - exp(-hdrColor * exposure);

    // also gamma correct while we're at it
    // const float gamma = 2.2;
    // result = pow(result, vec3(1.0 / gamma));
    FragColor = vec4(result, 1.0);
}