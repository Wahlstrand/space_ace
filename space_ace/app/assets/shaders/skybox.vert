#version 330 core

layout (location = 0) in vec3 inPos;

out VS_OUT {
   out vec3 texCoord;
} vs_out;

uniform mat4 view;
uniform mat4 proj;

void main()
{
    vec4 pos = proj * view * vec4(inPos, 1.0);
    gl_Position = pos.xyww; //xyww ensures that the map is drawn behind everything else
    vs_out.texCoord = inPos;
}
