#version 330 core

//Input
in VS_OUT {
   in vec3 texCoord;
} fs_in;

//Output
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

uniform samplerCube skybox;

void main()
{    
   FragColor = texture(skybox, fs_in.texCoord);

   float power = dot(FragColor, vec4(0.2126f, 0.7152f, 0.0722f, 0.0000f));
   BrightColor = min(1.0, power) * FragColor;
   BrightColor.w = 1.0f;
}



