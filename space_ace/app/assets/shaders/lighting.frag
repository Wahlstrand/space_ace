#version 330 core

in VS_OUT {
   in vec2 texCoord;
   in vec3 fragPos;
   in vec3 fragNormal;
} fs_in;

//Output
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

//Uniforms
struct Ambient
{
   float intensity;
   vec3 color;
};
struct Lights
{
   float power[64];
   vec3 color[64];
   vec3 pos[64];
   int nLights;
};

//Material
uniform sampler2D color;
uniform sampler2D emissive;
uniform Ambient ambient;
uniform Lights lights;

vec3 ambience(vec3 materialColor)
{
   return ambient.intensity * ambient.color * materialColor;
}
vec3 diffuse(vec3 materialColor)
{
   vec3 result = vec3(0.0);
   for(int n = 0; n < lights.nLights; n++)
   {
      //TODO: Add attenuation.
      vec3 vecToLight = lights.pos[n] - fs_in.fragPos;
      vec3 vecToLightNorm = normalize(vecToLight);
      float distance = length(vecToLight);
      float intensity = lights.power[n] * max(dot(vecToLightNorm, fs_in.fragNormal), 0.0f) / (distance * distance);
      vec3 contribution = intensity * lights.color[n] * materialColor;
      result += contribution;
   }
   return result;
}
void main()
{
   //TODO: Fix floating point emission maps, until then, scale it up
   vec3 emission3 = 20 * texture(emissive, fs_in.texCoord).xyz;

   // if(length(emission3) == 0.0f)
   // {
      vec3 color3 = texture(color, fs_in.texCoord).xyz;
      FragColor = vec4(ambience(color3) + diffuse(color3) + emission3, 1.0f);
   // }
   // else
   // {
   //    FragColor = vec4(emission3, 1.0f);
   // }

   //See https://en.wikipedia.org/wiki/Relative_luminance
   // if(dot(FragColor, vec4(0.2126f, 0.7152f, 0.0722f, 0.0000f)) > 10.0)
   // {
      float power = dot(FragColor, vec4(0.2126f, 0.7152f, 0.0722f, 0.0000f));
      BrightColor = min(1.0, power) * FragColor;
      BrightColor.w = 1.0f;
   // }
   // else
   // {
   //    BrightColor = vec4(0.0f, 0.0f, 0.0f, 1.0f);
   // }
}