#include "ecs/ecs.h"

#include <glad/glad.h>
#include <cmath>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include "ecs/ecs.h"

#include "sa/gfx/util/assimp/mesh.h"
#include "sa/gfx/util/gl/frame_buffer.h"
#include "sa/gfx/util/gl/image.h"
#include "sa/gfx/util/gl/render_buffer.h"
#include "sa/gfx/util/gl/shader_program.h"
#include "sa/gfx/util/gl/texture.h"
#include "sa/gfx/util/gl/util.h"
#include "sa/gfx/util/sdl/util.h"

#include "sa/gfx/components/camera.h"
#include "sa/gfx/components/camera_track.h"
#include "sa/gfx/components/lightsource.h"
#include "sa/gfx/components/model.h"
#include "sa/gfx/components/render_context.h"
#include "sa/gfx/systems/camera_movement.h"
#include "sa/gfx/systems/draw.h"
#include "sa/gfx/systems/process_events.h"

#include "sa/core/components/missile_physics.h"
#include "sa/core/components/player_control.h"
#include "sa/core/components/pos.h"
#include "sa/core/components/sensor.h"
#include "sa/core/components/signature.h"
#include "sa/core/components/spaceship_physics.h"
#include "sa/core/components/static_spin.h"
#include "sa/core/components/static_velocity.h"
#include "sa/core/components/tick.h"
#include "sa/core/components/weapon_controls.h"
#include "sa/core/components/weapon_launcher.h"
#include "sa/core/systems/missile_movement.h"
#include "sa/core/systems/spaceship_movement.h"
#include "sa/core/systems/static_rotation.h"
#include "sa/core/systems/tick.h"
#include "sa/core/systems/update_sensors.h"
#include "sa/core/systems/update_weapon_launchers.h"
#include "sa/core/util/index.h"

namespace {
using ECST = ecs::ECS<
    sa::core::component::Pos, sa::core::component::Tick,
    sa::core::component::StaticSpin, sa::core::component::StaticVelocity,
    sa::core::component::WeaponLauncher, sa::core::component::WeaponControls,
    sa::core::component::SpaceshipPhysics, sa::core::component::MissilePhysics,
    sa::core::component::PlayerControl, sa::core::component::Signature,
    sa::core::component::Sensor, sa::gfx::component::Model,
    sa::gfx::component::Lightsource, sa::gfx::component::Camera,
    sa::gfx::component::CameraTrack, sa::gfx::component::RenderContext>;

enum class TextureId {
  camo_stellar_jet_diffuse,
  camo_stellar_jet_emissive,
  micro_recon_diffuse,
  white_diffuse,   // TODO: don't use a file for this tex
  white_emissive,  // TODO: don't use a file for this tex
  missile_diffuse,
  missile_emissive,
};
using TextureIndex = sa::core::Index<TextureId, sa::gfx::gl::Texture>;
TextureIndex createTextureIndex() {
  TextureIndex index;

  using sa::gfx::gl::Image;
  using sa::gfx::gl::Texture;
  index.add(TextureId::camo_stellar_jet_diffuse,
            Texture::create2D(Image::load("CamoStellarJet.png")));
  index.add(TextureId::camo_stellar_jet_emissive,
            Texture::create2D(Image::load("CamoStellarJetEmissive.png")));
  index.add(TextureId::micro_recon_diffuse,
            Texture::create2D(Image::load("MicroRecon.png")));
  index.add(TextureId::white_diffuse,
            Texture::create2D(Image::load("white.png")));
  index.add(TextureId::white_emissive,
            Texture::create2D(Image::load("white.png")));
  index.add(TextureId::missile_diffuse,
            Texture::create2D(Image::load("missile.png")));
  index.add(TextureId::missile_emissive,
            Texture::create2D(Image::load("missile_emissive.png")));
  return index;
}

enum class MeshId {
  camo_stellar_jet_meshes,
  micro_recon_meshes,
  missile,
  white,  // TODO: Make generic cube
};
using MeshIndex = sa::core::Index<MeshId, std::vector<sa::gfx::gl::Mesh>>;
MeshIndex createMeshIndex() {
  MeshIndex index;
  index.add(MeshId::camo_stellar_jet_meshes,
            sa::gfx::assimp::load(true, 1.0f, "CamoStellarJet.obj"));
  index.add(MeshId::micro_recon_meshes,
            sa::gfx::assimp::load(true, 1.0f, "MicroRecon.obj"));
  index.add(MeshId::white, sa::gfx::assimp::load(true, 4.0f, "white.obj"));
  index.add(MeshId::missile, sa::gfx::assimp::load(true, 1.0f, "missile.obj"));

  return index;
}

std::vector<sa::gfx::component::Geometry> convert(
    const std::vector<sa::gfx::gl::Mesh> &meshes) {
  std::vector<sa::gfx::component::Geometry> res;
  std::transform(meshes.begin(), meshes.end(), std::back_inserter(res),
                 [](const sa::gfx::gl::Mesh &mesh) {
                   return sa::gfx::component::Geometry(mesh.vao(), mesh.nEbo());
                 });
  return res;
}
enum class ModelId {
  camo_stellar_jet,
  micro_recon,
  missile,
  white,
};
using ModelIndex = sa::core::Index<ModelId, sa::gfx::component::Model>;
ModelIndex createModelIndex(const MeshIndex &meshIdx,
                            const TextureIndex &textureIndex) {
  ModelIndex index;

  // CamoStellarJet
  {
    const auto &meshes = meshIdx.get(MeshId::camo_stellar_jet_meshes);
    const auto &diffuseMap =
        textureIndex.get(TextureId::camo_stellar_jet_diffuse);
    const auto &emissiveMap =
        textureIndex.get(TextureId::camo_stellar_jet_emissive);

    index.add(ModelId::camo_stellar_jet,
              sa::gfx::component::Model(convert(meshes), diffuseMap.handle(),
                                        emissiveMap.handle()));
  }

  // MicroRecon
  {
    const auto &meshes = meshIdx.get(MeshId::micro_recon_meshes);
    const auto &diffuseMap = textureIndex.get(TextureId::micro_recon_diffuse);

    index.add(
        ModelId::micro_recon,
        sa::gfx::component::Model(convert(meshes), diffuseMap.handle(), 0));
  }

  // Missile
  {
    const auto &meshes = meshIdx.get(MeshId::missile);
    const auto &diffuseMap = textureIndex.get(TextureId::missile_diffuse);
    const auto &emissiveMap = textureIndex.get(TextureId::missile_emissive);

    index.add(ModelId::missile,
              sa::gfx::component::Model(convert(meshes), diffuseMap.handle(),
                                        emissiveMap.handle()));
  }

  // Cube
  {
    const auto &meshes = meshIdx.get(MeshId::white);
    const auto &diffuseMap = textureIndex.get(TextureId::white_diffuse);
    const auto &emissiveMap = textureIndex.get(TextureId::white_emissive);

    index.add(ModelId::white,
              sa::gfx::component::Model(convert(meshes), diffuseMap.handle(),
                                        emissiveMap.handle()));
  }

  return index;
}

void addTick(ECST &ecs) {
  ecs.addComponents<sa::core::component::Tick>(ecs.addEntity(), {});
}

sa::gfx::gl::Mesh createSkybox() {
  static const std::vector<glm::vec3> vertices = {
      {-1.0f, 1.0f, -1.0f}, {-1.0f, -1.0f, -1.0f}, {-1.0f, 1.0f, 1.0f},
      {-1.0f, -1.0f, 1.0f}, {1.0f, -1.0f, -1.0f},  {1.0f, 1.0f, -1.0f},
      {1.0f, -1.0f, 1.0f},  {1.0f, 1.0f, 1.0f},
  };

  static const std::vector<GLuint> indices = {
      0, 1, 4, 4, 5, 0, 3, 1, 0, 0, 2, 3, 4, 6, 7, 7, 5, 4,
      3, 2, 7, 7, 6, 3, 0, 5, 7, 7, 2, 0, 1, 3, 4, 4, 3, 6,
  };

  GLuint vbo, vao, ebo;
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(),
               vertices.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(),
               indices.data(), GL_STATIC_DRAW);

  glVertexAttribPointer(
      0,                                         // location of pos in the VS
      decltype(vertices)::value_type::length(),  // n elements in this object
      GL_FLOAT,                                  // data type
      GL_FALSE,                                  // normalized
      sizeof(decltype(vertices)::value_type),    // stride
      (void *)offsetof(decltype(vertices)::value_type, x));  // offset
  glEnableVertexAttribArray(0);
  glBindVertexArray(0);

  return sa::gfx::gl::Mesh(vao, vbo, ebo, static_cast<GLuint>(indices.size()));
}

void addRenderContext(ECST &ecs, SDL_Window *pWindow) {
  const int SCR_WIDTH = 1280;  // TODO: Remove hardcodedness
  const int SCR_HEIGHT = 720;  // TODO: Remove hardcodedness

  // Light rendering
  sa::gfx::gl::Texture lightingOutputColorBuffer;
  glBindTexture(GL_TEXTURE_2D, lightingOutputColorBuffer.handle());
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA,
               GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  sa::gfx::gl::Texture brightLightingOutputColorBuffer;
  glBindTexture(GL_TEXTURE_2D, brightLightingOutputColorBuffer.handle());
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA,
               GL_FLOAT, NULL);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  sa::gfx::gl::RenderBuffer lightingDepthBuffer;
  glBindRenderbuffer(GL_RENDERBUFFER, lightingDepthBuffer.handle());
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCR_WIDTH,
                        SCR_HEIGHT);

  sa::gfx::gl::FrameBuffer lightingFrameBuffer;
  glBindFramebuffer(GL_FRAMEBUFFER, lightingFrameBuffer.handle());
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                         lightingOutputColorBuffer.handle(), 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D,
                         brightLightingOutputColorBuffer.handle(), 0);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                            GL_RENDERBUFFER, lightingDepthBuffer.handle());

  const GLuint attachments[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
  glDrawBuffers(2, attachments);

  const auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    throw std::runtime_error("Incomplete framebuffer");
  }

  // Bloom
  std::array<sa::gfx::gl::Texture, 2> blurColorBuffers;
  std::array<sa::gfx::gl::FrameBuffer, 2> blurFrameBuffers;
  for (GLuint n = 0; n < 2; n++) {
    glBindTexture(GL_TEXTURE_2D, blurColorBuffers[n].handle());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0,
                 GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindFramebuffer(GL_FRAMEBUFFER, blurFrameBuffers[n].handle());
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                           blurColorBuffers[n].handle(), 0);
  }

  // Finalization
  struct VertexData {
    glm::vec3 m_position;
    glm::vec2 m_texCoords;
  };
  static const std::vector<VertexData> vertices = {
      {{1.0f, 1.0f, 0.0f}, {1.0f, 1.0f}},    // top right
      {{1.0f, -1.0f, 0.0f}, {1.0f, 0.0f}},   // bottom right
      {{-1.0f, -1.0f, 0.0f}, {0.0f, 0.0f}},  // bottom left
      {{-1.0f, 1.0f, 0.0f}, {0.0f, 1.0f}}};  // top left

  static const std::vector<GLuint> indices = {
      0, 1, 3,  // first Triangle
      1, 2, 3   // second Triangle
  };

  GLuint vbo, vao, ebo;
  glGenVertexArrays(1, &vao);
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  glBindVertexArray(vao);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * vertices.size(),
               vertices.data(), GL_STATIC_DRAW);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(),
               indices.data(), GL_STATIC_DRAW);

  glVertexAttribPointer(
      0,                                           // location of pos in the VS
      decltype(VertexData::m_position)::length(),  // n elements in this object
      GL_FLOAT,                                    // data type
      GL_FALSE,                                    // normalized
      sizeof(VertexData),                          // stride
      (void *)offsetof(VertexData, m_position));   // offset

  glVertexAttribPointer(
      1,                                            // location of pos in the VS
      decltype(VertexData::m_texCoords)::length(),  // n elements in this object
      GL_FLOAT,                                     // data type
      GL_FALSE,                                     // normalized
      sizeof(VertexData),                           // stride
      (void *)offsetof(VertexData, m_texCoords));   // offset
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glBindVertexArray(0);

  sa::gfx::gl::Mesh outputRect(vao, vbo, ebo,
                               static_cast<GLuint>(indices.size()));

  sa::gfx::component::RenderContext component = {
      // Window
      .m_pWindow = pWindow,

      // Light rendering
      .m_lightingFrameBuffer = std::move(lightingFrameBuffer),
      .m_lightingOutputColorBuffer = std::move(lightingOutputColorBuffer),
      .m_brightLightingOutputColorBuffer =
          std::move(brightLightingOutputColorBuffer),
      .m_lightingDepthBuffer = std::move(lightingDepthBuffer),
      .m_lightingShader =
          sa::gfx::gl::ShaderProgram::load("lighting.vert", "lighting.frag"),

      // Bloom buffers
      .m_nBlurPasses = 2,
      .m_blurColorBuffers = std::move(blurColorBuffers),
      .m_blurFrameBuffers = std::move(blurFrameBuffers),
      .m_blurShader =
          sa::gfx::gl::ShaderProgram::load("blur.vert", "blur.frag"),

      // Skybox
      .m_skyboxMesh = createSkybox(),
      .m_skyboxShader =
          sa::gfx::gl::ShaderProgram::load("skybox.vert", "skybox.frag"),
      .m_skyboxTex = sa::gfx::gl::Texture::createCubeMap(
          sa::gfx::gl::Image::load("backgrounds/teal/front.png"),
          sa::gfx::gl::Image::load("backgrounds/teal/back.png"),
          sa::gfx::gl::Image::load("backgrounds/teal/right.png"),
          sa::gfx::gl::Image::load("backgrounds/teal/left.png"),
          sa::gfx::gl::Image::load("backgrounds/teal/bot.png"),
          sa::gfx::gl::Image::load("backgrounds/teal/top.png")),

      // Finalization
      .m_renderShader =
          sa::gfx::gl::ShaderProgram::load("finalize.vert", "finalize.frag"),
      .m_renderRect = std::move(outputRect),
  };

  ecs.addComponents<sa::gfx::component::RenderContext>(ecs.addEntity(),
                                                       std::move(component));
}
void addCamera(ECST &ecs) {
  ecs.addComponents<sa::core::component::Pos, sa::gfx::component::Camera>(
      ecs.addEntity(), {{-40.0f, 0.0f, 0.0f}, glm::quat()},
      {});
}
void addLightsource(ECST &ecs, const ModelIndex &modelIndex) {
  for (int n = 0; n < 8; n++) {
    for (int m = 0; m < 8; m++) {
      ecs.addComponents<sa::core::component::Pos,
                        sa::gfx::component::Lightsource,
                        sa::gfx::component::Model>(
          ecs.addEntity(), {{200 * n, 200 * m, -5.0f}, glm::quat()},
          {{1.0f, 1.0f, 1.0f}, 50.0f}, modelIndex.get(ModelId::white));
    }
  }
}
void addPlayer(ECST &ecs, const ModelIndex &modelIndex) {
  ecs.addComponents<sa::core::component::Pos, sa::core::component::Sensor,
                    sa::core::component::PlayerControl,
                    sa::core::component::WeaponLauncher,
                    sa::core::component::WeaponControls,
                    sa::core::component::SpaceshipPhysics,
                    sa::gfx::component::CameraTrack, sa::gfx::component::Model>(
      ecs.addEntity(), {{-15.0f, 0.0f, 0.0f}, {}}, {}, {}, {}, {}, {}, {},
      modelIndex.get(ModelId::camo_stellar_jet));
}

void setup(ECST &ecs, SDL_Window *pWindow, const ModelIndex &modelIndex) {
  addTick(ecs);
  addRenderContext(ecs, pWindow);
  addCamera(ecs);
  addLightsource(ecs, modelIndex);
  addPlayer(ecs, modelIndex);
}

void onLaunch(ModelIndex &modelIdx, ECST &ecs, ecs::EntityId missileId) {
  ecs.addComponents<sa::gfx::component::Model>(missileId,
                                               modelIdx.get(ModelId::missile));
}
}  // namespace

int main(int, char *[]) {
  // SDL/GL Setup
  const int w = 1280;
  const int h = 720;
  try {
    sa::gfx::sdl::init();
    auto upWindow = sa::gfx::sdl::createWindow(w, h);
    auto upContext = sa::gfx::sdl::createContext(upWindow.get());
    sa::gfx::gl::initGlad();
    sa::gfx::gl::init(w, h);

    // lifetime management
    auto textureIndex = createTextureIndex();
    auto meshIndex = createMeshIndex();
    auto modelIndex = createModelIndex(meshIndex, textureIndex);

    ECST ecs;
    setup(ecs, upWindow.get(), modelIndex);

    using namespace std::placeholders;
    sa::core::systems::Tick<ECST> tick(ecs);
    sa::core::systems::UpdateWeaponLaunchers<ECST> updateWeaponLaunchers(
        ecs, std::bind(onLaunch, std::ref(modelIndex), _1, _2));
    sa::core::systems::UpdateSensors<ECST> updateSensors(ecs);
    sa::core::systems::MissileMovement<ECST> missileMovement(ecs);
    sa::core::systems::SpaceshipMovement<ECST> spaceshipMovement(ecs);
    sa::core::systems::StaticRotation<ECST> staticRotation(ecs);
    sa::gfx::systems::CameraMovement<ECST> cameraMovement(ecs);
    sa::gfx::systems::ProcessEvents<ECST> processEvents(ecs);
    sa::gfx::systems::Draw<ECST> draw(ecs);
    while (true) {
      tick.update();
      updateWeaponLaunchers.update();
      updateSensors.update();
      missileMovement.update();
      spaceshipMovement.update();
      staticRotation.update();
      cameraMovement.update();
      processEvents.update();
      draw.update();
    }
  } catch (const std::exception &e) {
    std::cerr << "Unhandled error: " << e.what() << '\n';
  }
  return 0;
}
