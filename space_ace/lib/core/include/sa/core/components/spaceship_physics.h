#pragma once

namespace sa::core::component {
struct SpaceshipPhysics {
  // State
  float m_speed = 2.5f;
  float m_roll = 0.0f;  // These are a bit redundant with pos but
  float m_yaw = 0.0f;   // it simplifies math and makes it easier to prevent
                       // angular drift

  // Specs
  float m_maxTurnRate = 0.6f / 60.0f;                   // 0.6 deg/tick
  float m_maxRoll = 1.3089969389957471826927680763665;  // 75 deg
  float m_responsiveness = 0.03f;

  // Controls
  enum class TurnCommand {
    left,
    right,
    none,
  };
  TurnCommand m_cmd = TurnCommand::none;
};
}  // namespace sa::core::component