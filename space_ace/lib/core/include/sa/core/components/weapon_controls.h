#pragma once

namespace sa::core::component {
struct WeaponControls {
  enum class Command { idle, fire };
  Command m_cmd = Command::idle;
};
}  // namespace sa::core::component