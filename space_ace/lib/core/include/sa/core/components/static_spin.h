#pragma once

#include <glm/vec3.hpp>

namespace sa::core::component {
// An angular velocity in world space
struct StaticSpin {
  glm::vec3 m_t;
};
}  // namespace sa::core::component