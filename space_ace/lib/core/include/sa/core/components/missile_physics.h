#pragma once

namespace sa::core::component {
struct MissilePhysics {
  // Config
  float m_acceleration = 0.04f;
  float m_topSpeed = 10.0f;

  // State
  float m_speed = 0.0f;
};
}  // namespace sa::core::component