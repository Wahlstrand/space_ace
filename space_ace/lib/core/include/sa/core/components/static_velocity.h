#pragma once

#include <glm/vec3.hpp>

namespace sa::core::component {
struct StaticVelocity {
  glm::vec3 m_v;
};
}  // namespace sa::core::component