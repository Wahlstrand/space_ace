#pragma once

#include <vector>
#include "ecs/ecs_fwd.h"

namespace sa::core::component {
struct Sensor {
  // Config
  float m_fov = 0.7f;  // Angle in radians from forward
  float m_range = 500.0f;

  // State
  std::vector<ecs::EntityId> m_tracks;
};
}  // namespace sa::core::component