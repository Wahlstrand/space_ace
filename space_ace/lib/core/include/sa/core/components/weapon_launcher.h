#pragma once

#include <cstdint>
#include <glm/vec3.hpp>

namespace sa::core::component {
struct WeaponLauncher {
  uint64_t m_availableAt = 0;
  uint64_t m_cooldown = 60;

  glm::vec3 m_launchOffsetBody = {0.0f, 0.0f, 1.0f};
};
}  // namespace sa::core::component