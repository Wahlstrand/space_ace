#pragma once

#include <cstdint>

namespace sa::core::component {
// Position in world space
struct Tick {
  uint64_t m_frame = 0;  // Game time
};
}  // namespace sa::core::component