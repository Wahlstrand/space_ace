#pragma once

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

namespace sa::core::component {
// Position in world space
struct Pos {
  glm::vec3 m_r;
  glm::quat m_quat;
};
}  // namespace sa::core::component