#pragma once

#include <map>
#include <sstream>
#include <stdexcept>

namespace sa::core {
template <typename IdT, typename TypeT>
class Index {
 public:
  Index() = default;
  Index(const Index &other) = delete;
  Index(Index &&other) : m_items(std::move(other.m_items)) {}
  ~Index() = default;

  void add(IdT id, TypeT &&item) { m_items.emplace(id, std::move(item)); }
  const TypeT &get(IdT id) const {
    auto itr = m_items.find(id);
    if (itr == m_items.end()) {
      std::stringstream ss;
      ss << "Model " << static_cast<int>(id) << " does not exist";
      throw std::runtime_error(ss.str());
    }
    return itr->second;
  }

 private:
  std::map<IdT, TypeT> m_items;
};
}  // namespace sa::core