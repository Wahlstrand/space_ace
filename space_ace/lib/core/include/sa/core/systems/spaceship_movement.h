#pragma once

#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "sa/core/components/pos.h"
#include "sa/core/components/spaceship_physics.h"

namespace sa::core::systems {
namespace {
float dir(component::SpaceshipPhysics::TurnCommand cmd) {
  switch (cmd) {
    case component::SpaceshipPhysics::TurnCommand::left:
      return -1.0f;
    case component::SpaceshipPhysics::TurnCommand::right:
      return 1.0f;
    case component::SpaceshipPhysics::TurnCommand::none:
      return 0.0f;
  }
  throw std::runtime_error("Unhandled enum value");
}
}  // namespace

template <typename ECST>
class SpaceshipMovement : public ecs::ISystem {
 public:
  SpaceshipMovement(ECST &ecs) : m_ecs(ecs), m_tracker(ecs) {}
  void update() final {
    m_tracker.foreach ([](ecs::EntityId, core::component::Pos &pos,
                          core::component::SpaceshipPhysics &physics) {
      // roll
      const float tgt = physics.m_maxRoll * dir(physics.m_cmd);
      physics.m_roll = (1.0f - physics.m_responsiveness) * physics.m_roll +
                       physics.m_responsiveness * tgt;

      // turn
      const float turnRate =
          physics.m_roll / physics.m_maxRoll * physics.m_maxTurnRate;
      physics.m_yaw += turnRate;

      const glm::vec3 x(1.0f, 0.0f, 0.0f);
      const glm::vec3 z(0.0f, 0.0f, 1.0f);
      pos.m_quat =
          glm::angleAxis(physics.m_yaw, z) * glm::angleAxis(physics.m_roll, x);
      pos.m_r += pos.m_quat * x * physics.m_speed;
    });
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, component::Pos, component::SpaceshipPhysics>
      m_tracker;
};
}  // namespace sa::core::systems