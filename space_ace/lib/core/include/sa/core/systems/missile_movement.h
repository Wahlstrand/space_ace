#pragma once

#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "sa/core/components/missile_physics.h"
#include "sa/core/components/pos.h"

namespace sa::core::systems {
template <typename ECST>
class MissileMovement : public ecs::ISystem {
 public:
  MissileMovement(ECST &ecs) : m_ecs(ecs), m_tracker(ecs) {}
  void update() final {
    m_tracker.foreach ([](ecs::EntityId, core::component::Pos &pos,
                          core::component::MissilePhysics &physics) {
      const glm::vec3 x(1.0f, 0.0f, 0.0f);
      physics.m_speed = std::min<float>(
          physics.m_topSpeed, physics.m_speed + physics.m_acceleration);
      pos.m_r += physics.m_speed * (pos.m_quat * x);
    });
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, component::Pos, component::MissilePhysics> m_tracker;
};
}  // namespace sa::core::systems