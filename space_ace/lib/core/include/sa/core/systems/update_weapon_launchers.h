#pragma once
#include <functional>
#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/missile_physics.h"
#include "sa/core/components/pos.h"
#include "sa/core/components/tick.h"
#include "sa/core/components/weapon_controls.h"
#include "sa/core/components/weapon_launcher.h"

namespace sa::core::systems {
template <typename ECST>
class UpdateWeaponLaunchers : public ecs::ISystem {
 public:
  UpdateWeaponLaunchers(ECST &ecs,
                        const std::function<void(ECST &ecs, ecs::EntityId id)>
                            &onLaunch)  // Can be used to add graphics etc
      : m_ecs(ecs),
        m_onLaunch(onLaunch),
        m_weaponTracker(ecs),
        m_tickTracker(ecs) {}
  void update() final {
    const auto &tick =
        m_ecs.template getComponent<component::Tick>(m_tickTracker.track());

    m_weaponTracker.foreach ([this, tick](ecs::EntityId, component::Pos &pos,
                                          component::WeaponLauncher &launcher,
                                          component::WeaponControls &controls) {
      if (controls.m_cmd != component::WeaponControls::Command::fire) {
        // Not firing
        return;
      }

      if (tick.m_frame < launcher.m_availableAt) {
        // Cooling down
        return;
      }

      launcher.m_availableAt = tick.m_frame + launcher.m_cooldown;

      const auto launchOffsetWorld = pos.m_quat * launcher.m_launchOffsetBody;
      const auto launchPosWorld = pos.m_r + launchOffsetWorld;

      auto missile = m_ecs.addEntity();

      m_ecs.template addComponents<component::Pos, component::MissilePhysics>(
          missile, {launchPosWorld, pos.m_quat},
          {.m_speed = 1.75f});  // TODO: Sync with launcher

      m_onLaunch(m_ecs, missile);
    });
  }

 private:
  ECST &m_ecs;
  std::function<void(ECST &ecs, ecs::EntityId id)> m_onLaunch;
  ecs::EntityTracker<ECST, component::Pos, component::WeaponLauncher,
                     component::WeaponControls>
      m_weaponTracker;
  ecs::SingleEntityTracker<ECST, component::Tick> m_tickTracker;
};
}  // namespace sa::core::systems