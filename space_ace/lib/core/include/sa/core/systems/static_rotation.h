#pragma once

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "sa/core/components/pos.h"
#include "sa/core/components/static_spin.h"

namespace sa::core::systems {
template <typename ECST>
class StaticRotation : public ecs::ISystem {
 public:
  StaticRotation(ECST &ecs) : m_ecs(ecs), m_tracker(ecs) {}
  void update() final {
    for (auto id : m_tracker.tracks()) {
      auto &pos = m_ecs.template getComponent<component::Pos>(id);
      auto &spin = m_ecs.template getComponent<component::StaticSpin>(id);
      pos.m_quat =
          glm::angleAxis(glm::length(spin.m_t), glm::normalize(spin.m_t)) *
          pos.m_quat;
      pos.m_quat = glm::normalize(pos.m_quat);
    }
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, component::Pos, component::StaticSpin> m_tracker;
};
}  // namespace sa::core::systems