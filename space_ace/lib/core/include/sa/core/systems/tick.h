#pragma once

#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/tick.h"

namespace sa::core::systems {
template <typename ECST>
class Tick : public ecs::ISystem {
 public:
  Tick(ECST &ecs) : m_ecs(ecs), m_tracker(ecs) {}
  void update() final {
    auto &tick =
        m_ecs.template getComponent<component::Tick>(m_tracker.track());
    tick.m_frame++;
  }

 private:
  ECST &m_ecs;
  ecs::SingleEntityTracker<ECST, component::Tick> m_tracker;
};
}  // namespace sa::core::systems