#pragma once

#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "sa/core/components/pos.h"
#include "sa/core/components/sensor.h"
#include "sa/core/components/signature.h"

namespace sa::core::systems {
template <typename ECST>
class UpdateSensors : public ecs::ISystem {
 public:
  UpdateSensors(ECST &ecs)
      : m_ecs(ecs), m_sensorTracker(ecs), m_signatureTracker(ecs) {}
  void update() final {
    m_sensorTracker.foreach ([&](ecs::EntityId sensorId,
                                 core::component::Pos &sensorPos,
                                 core::component::Sensor &sensor) {
      sensor.m_tracks.clear();
      m_signatureTracker.foreach ([&](ecs::EntityId signatureId,
                                      core::component::Pos &signaturePos,
                                      core::component::Signature &signature) {
        if (sensorId == signatureId) {
          return;
        }

        if (!detectable(sensorPos, sensor, signaturePos, signature)) {
          return;
        }

        sensor.m_tracks.push_back(signatureId);
      });
    });
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, component::Pos, component::Sensor> m_sensorTracker;
  ecs::EntityTracker<ECST, component::Pos, component::Signature>
      m_signatureTracker;

  bool detectable(core::component::Pos &sensorPos,
                  core::component::Sensor &sensor,
                  core::component::Pos &signaturePos,
                  core::component::Signature &signature) {
    // Range check
    const glm::vec3 sensorToSignature = signaturePos.m_r - sensorPos.m_r;
    if (glm::length(sensorToSignature) >=
        sensor.m_range * signature.m_detectability) {
      return false;
    }

    // FoV check
    const glm::vec3 sensorFwd = sensorPos.m_quat * glm::vec3(1.0f, 0.0f, 0.0f);
    const glm::vec3 targetDir = glm::normalize(sensorToSignature);
    if (glm::dot(sensorFwd, targetDir) < std::cos(sensor.m_fov)) {
      return false;
    }

    return true;
  }
};
}  // namespace sa::core::systems