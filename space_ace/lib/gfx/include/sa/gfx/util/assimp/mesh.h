#pragma once

#include <string>
#include <vector>
#include "sa/gfx/util/gl/mesh.h"

namespace sa::gfx::assimp {
std::vector<gl::Mesh> load(bool center, float scale, const std::string &path);
}  // namespace sa::gfx::assimp