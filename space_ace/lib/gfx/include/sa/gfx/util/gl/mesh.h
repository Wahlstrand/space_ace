#pragma once

#include <glad/glad.h>

namespace sa::gfx::gl {
class Mesh final {
 public:
  Mesh(GLuint vao, GLuint vbo, GLuint ebo, GLuint nEbo);
  Mesh(Mesh &&other);
  ~Mesh();

  Mesh &operator=(Mesh &&other);

  GLuint vao() const;
  GLuint nEbo() const;

 private:
  GLuint m_vao;
  GLuint m_vbo;
  GLuint m_ebo;
  GLuint m_nEbo;
};

}  // namespace sa::gfx::gl