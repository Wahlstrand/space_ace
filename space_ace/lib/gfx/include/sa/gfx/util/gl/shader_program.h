#pragma once

#include <glad/glad.h>
#include <string>

namespace sa::gfx::gl {
// These helper classes are pretty much copy-pasted; find a common
// implementation
class ShaderProgram final {
 public:
  ShaderProgram();
  ShaderProgram(ShaderProgram &&other);
  ~ShaderProgram();

  ShaderProgram &operator=(ShaderProgram &&other);

  GLuint handle() const;

  static ShaderProgram load(const std::string &vsFile,
                            const std::string &fsFile);

 private:
  GLuint m_handle;
};

}  // namespace sa::gfx::gl