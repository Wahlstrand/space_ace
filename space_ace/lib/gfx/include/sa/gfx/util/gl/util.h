#pragma once

#include <glad/glad.h>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>
#include <string>

namespace sa::gfx::gl {
void init(GLuint w, GLuint h);
void initGlad();

void setInt(GLuint program, const std::string& uniform, GLint val);
void setFloat(GLuint program, const std::string& uniform, float val);
void setFloats(GLuint program, const std::string& uniform, const float* pVals,
               GLuint nVals);
void setVec3(GLuint program, const std::string& uniform, const glm::vec3& val);
void setVec3s(GLuint program, const std::string& uniform,
              const glm::vec3* pVals, GLuint nVals);
void setVec4(GLuint program, const std::string& uniform, const glm::vec4& val);
void setMat4(GLuint program, const std::string& uniform, const glm::mat4& val);
}  // namespace sa::gfx::gl