#pragma once

#include <glad/glad.h>
#include "sa/gfx/util/gl/image_fwd.h"

namespace sa::gfx::gl {

// These helper classes are pretty much copy-pasted; find a common
// implementation
class Texture final {
 public:
  Texture();
  Texture(GLuint id);  // Claims ownership of the texture
  Texture(Texture &&other);
  ~Texture();

  Texture &operator=(Texture &&other);

  GLuint handle() const;

  static Texture create2D(Image const &img);
  static Texture createCubeMap(Image const &posX, Image const &negX,
                               Image const &posY, Image const &negY,
                               Image const &posZ, Image const &negZ);

 private:
  GLuint m_handle;
};
}  // namespace sa::gfx::gl