#pragma once

#include <glad/glad.h>

namespace sa::gfx::gl {
// These helper classes are pretty much copy-pasted; find a common
// implementation
class FrameBuffer final {
 public:
  FrameBuffer();
  FrameBuffer(GLuint handle);
  FrameBuffer(FrameBuffer &&other);
  ~FrameBuffer();

  FrameBuffer &operator=(FrameBuffer &&other);

  GLuint handle() const;

 private:
  GLuint m_handle;
};

}  // namespace sa::gfx::gl