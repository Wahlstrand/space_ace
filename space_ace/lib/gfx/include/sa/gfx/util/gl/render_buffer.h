#pragma once

#include <glad/glad.h>

namespace sa::gfx::gl {
// These helper classes are pretty much copy-pasted; find a common
// implementation
class RenderBuffer final {
 public:
  RenderBuffer();
  RenderBuffer(GLuint handle);  // Claims ownership of the texture
  RenderBuffer(RenderBuffer &&other);
  ~RenderBuffer();

  RenderBuffer &operator=(RenderBuffer &&other);

  GLuint handle() const;

 private:
  GLuint m_handle;
};
}  // namespace sa::gfx::gl