#pragma once

#include <SDL.h>
#include <glm/vec2.hpp>
#include <memory>

namespace sa::gfx::sdl {
void init();

typedef std::unique_ptr<SDL_Window, void (*)(SDL_Window*)> SDLWindowUP;
typedef std::unique_ptr<void, void (*)(void*)> SDLGLContextUP;

SDLWindowUP createWindow(int w, int h);
SDLGLContextUP createContext(SDL_Window* pWindow);

glm::ivec2 size(SDL_Window* pWindow);

}  // namespace sa::gfx::sdl