#pragma once

#include <glm/gtc/quaternion.hpp>
#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/pos.h"
#include "sa/gfx/components/camera.h"
#include "sa/gfx/components/camera_track.h"

namespace sa::gfx::systems {
template <typename ECST>
class CameraMovement : public ecs::ISystem {
 public:
  CameraMovement(ECST &ecs)
      : m_ecs(ecs), m_trackTracker(ecs), m_cameraTracker(ecs) {}
  void update() final {
    const auto cameraId = m_cameraTracker.track();
    auto &camera =
        m_ecs.template getComponent<gfx::component::Camera>(cameraId);

    // Let camera gently go back to default orientation after drag release
    if (!camera.m_dragged) {
      const static glm::quat identity = glm::quat(glm::vec3());
      camera.m_trackball = glm::mix(camera.m_trackball, identity, 0.1f);
    }

    const auto &trackPos = m_ecs.template getComponent<core::component::Pos>(
        m_trackTracker.track());
    const glm::vec3 trackX = trackPos.m_quat * glm::vec3(1.0f, 0.0f, 0.0f);
    const glm::quat cameraYaw = glm::angleAxis(glm::atan(trackX.y, trackX.x),
                                               glm::vec3(0.0f, 0.0f, 1.0f));
    const glm::quat cameraPitch =
        glm::angleAxis(camera.m_pitch, glm::vec3(0.0f, 1.0f, 0.0f));

    const auto camRot = cameraYaw * camera.m_trackball * cameraPitch;
    const auto camForward = camRot * glm::vec3(1.0f, 0.0f, 0.0f);

    auto &cameraPos =
        m_ecs.template getComponent<core::component::Pos>(cameraId);
    cameraPos.m_quat = camRot;
    cameraPos.m_r = glm::vec3(0.0, 0.0, camera.m_zOffset) +  trackPos.m_r - (camForward * camera.m_distance);
  }

 private:
  ECST &m_ecs;
  ecs::SingleEntityTracker<ECST, core::component::Pos, component::CameraTrack>
      m_trackTracker;
  ecs::SingleEntityTracker<ECST, core::component::Pos, component::Camera>
      m_cameraTracker;
};
}  // namespace sa::gfx::systems