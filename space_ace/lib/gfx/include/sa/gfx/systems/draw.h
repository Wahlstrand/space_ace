#pragma once

#include "ecs/ecs.h"
#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/pos.h"
#include "sa/gfx/components/camera.h"
#include "sa/gfx/components/lightsource.h"
#include "sa/gfx/components/model.h"
#include "sa/gfx/components/render_context.h"
#include "sa/gfx/util/gl/util.h"
#include "sa/gfx/util/sdl/util.h"

namespace sa::gfx::systems {
glm::mat4 projectionMatrix(float fov, float zNear, float zFar, int w, int h);
glm::mat4 viewMatrix(const glm::vec3 &cameraPos, const glm::quat &cameraRot);

template <typename ECST>
class Draw : public ecs::ISystem {
 public:
  Draw(ECST &ecs)
      : m_ecs(ecs),
        m_modelTracker(ecs),
        m_lightTracker(ecs),
        m_renderContextTrakcer(ecs),
        m_cameraTracker(ecs) {}

  void update() final {
    component::RenderContext &context =
        m_ecs.template getComponent<component::RenderContext>(
            m_renderContextTrakcer.track());
    auto &camera =
        m_ecs.template getComponent<component::Camera>(m_cameraTracker.track());
    auto &cameraPos = m_ecs.template getComponent<core::component::Pos>(
        m_cameraTracker.track());

    const auto size = sdl::size(context.m_pWindow);

    renderLights(context, camera, cameraPos, size.x,
                 size.y);  // Renders lighting to context.m_lightingFrameBuffer
    renderBloom(context, size.x,
                size.y);  // Renders bloom to context.m_blurColorBuffers[1]
    present(context, size.x, size.y);  //
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, core::component::Pos, component::Model>
      m_modelTracker;
  ecs::EntityTracker<ECST, core::component::Pos, component::Lightsource>
      m_lightTracker;
  ecs::SingleEntityTracker<ECST, component::RenderContext>
      m_renderContextTrakcer;
  ecs::SingleEntityTracker<ECST, core::component::Pos, component::Camera>
      m_cameraTracker;

  void renderLights(const component::RenderContext &context,
                    const component::Camera &camera,
                    const core::component::Pos &cameraPos, int w, int h) {
    glBindFramebuffer(GL_FRAMEBUFFER, context.m_lightingFrameBuffer.handle());
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Draw lighting
    const GLuint lightingShader = context.m_lightingShader.handle();
    glUseProgram(lightingShader);
    const glm::mat4 view = viewMatrix(cameraPos.m_r, cameraPos.m_quat);
    const glm::mat4 proj =
        projectionMatrix(camera.m_fov, camera.m_zNear, camera.m_zFar, w, h);
    gl::setMat4(lightingShader, "view", view);
    gl::setMat4(lightingShader, "proj", proj);

    gl::setFloat(lightingShader, "ambient.intensity", 0.7f);
    gl::setVec3(lightingShader, "ambient.color", {1.0f, 1.0f, 1.0f});

    // Add point lights, maximum 64 right now.
    constexpr GLuint maxLights = 64;
    std::array<float, maxLights> power;
    std::array<glm::vec3, maxLights> color;
    std::array<glm::vec3, maxLights> pos;
    GLuint nLights = 0;
    m_lightTracker.foreach ([&](ecs::EntityId, const core::component::Pos &cPos,
                                const component::Lightsource &cLs) {
      if (nLights >= maxLights)  // Limited number of lights allowed for now
      {
        std::cerr << "Skipping extra lights!\n";
        return;
      }

      power[nLights] = cLs.m_power;
      color[nLights] = cLs.m_color;
      pos[nLights] = cPos.m_r;
      nLights++;
    });

    gl::setFloats(lightingShader, "lights.power", power.data(), nLights);
    gl::setVec3s(lightingShader, "lights.color", color.data(), nLights);
    gl::setVec3s(lightingShader, "lights.pos", pos.data(), nLights);
    gl::setInt(lightingShader, "lights.nLights", nLights);

    // TODO: Try to batch these so we don't have to swap around VAOs so much?
    m_modelTracker.foreach ([=](ecs::EntityId,
                                core::component::Pos &posComponent,
                                gfx::component::Model &modelComponent) {
      const glm::mat4 trans =
          glm::translate(glm::identity<glm::mat4>(), posComponent.m_r);
      const glm::mat4 rot = glm::mat4_cast(posComponent.m_quat);
      const glm::mat4 model = trans * rot;
      gl::setMat4(lightingShader, "model", model);

      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, modelComponent.m_diffuse);
      gl::setInt(lightingShader, "color", 0);

      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, modelComponent.m_emissive);
      gl::setInt(lightingShader, "emissive", 1);

      for (const auto &geometry : modelComponent.m_geometry) {
        glBindVertexArray(geometry.m_vao);
        glDrawElements(GL_TRIANGLES, geometry.m_nEbo, GL_UNSIGNED_INT, 0);
      }

      glBindVertexArray(0);
    });

    // Draw skybox
    const GLuint skyboxShader = context.m_skyboxShader.handle();
    glUseProgram(skyboxShader);
    gl::setMat4(skyboxShader, "view",
                glm::mat4(glm::mat3(view)));  // Remove translation
    gl::setMat4(skyboxShader, "proj", proj);
    glDepthFunc(GL_LEQUAL);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, context.m_skyboxTex.handle());
    draw(context.m_skyboxMesh);
    glDepthFunc(GL_LESS);

    // cleanup
    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
  void renderBloom(const component::RenderContext &context, int w, int h) {
    glUseProgram(context.m_blurShader.handle());
    gl::setInt(context.m_blurShader.handle(), "tex", 0);

    const GLuint horizontalOutputBuffer =
        context.m_blurColorBuffers[0].handle();
    const GLuint verticalOutputBuffer = context.m_blurColorBuffers[1].handle();
    for (GLuint n = 0; n < context.m_nBlurPasses; n++) {
      // Horizontal pass
      glBindFramebuffer(GL_FRAMEBUFFER, context.m_blurFrameBuffers[0].handle());
      glViewport(0, 0, w, h);
      gl::setInt(context.m_blurShader.handle(), "horizontal", true);
      const GLuint horizontalTarget =
          (n == 0 ? context.m_brightLightingOutputColorBuffer.handle()
                  : verticalOutputBuffer);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, horizontalTarget);
      draw(context.m_renderRect);

      // Vertical pass
      glBindFramebuffer(GL_FRAMEBUFFER, context.m_blurFrameBuffers[1].handle());
      glViewport(0, 0, w, h);
      gl::setInt(context.m_blurShader.handle(), "horizontal", false);
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, horizontalOutputBuffer);
      draw(context.m_renderRect);
    }

    // cleanup
    glUseProgram(0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  void present(const component::RenderContext &context, int w, int h) {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, w, h);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(context.m_renderShader.handle());

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, context.m_lightingOutputColorBuffer.handle());
    gl::setInt(context.m_renderShader.handle(), "lighting", 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, context.m_blurColorBuffers[1].handle());
    gl::setInt(context.m_renderShader.handle(), "bloom", 1);
    draw(context.m_renderRect);
    SDL_GL_SwapWindow(context.m_pWindow);
  }

  void draw(const gl::Mesh &mesh) {
    glBindVertexArray(mesh.vao());
    glDrawElements(GL_TRIANGLES, mesh.nEbo(), GL_UNSIGNED_INT, 0);
  }
};
}  // namespace sa::gfx::systems