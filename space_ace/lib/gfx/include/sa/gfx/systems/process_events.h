#pragma once

#include <SDL.h>
#include <algorithm>
#include "ecs/entity_tracker.h"
#include "ecs/isystem.h"
#include "ecs/single_entity_tracker.h"
#include "sa/core/components/player_control.h"
#include "sa/core/components/spaceship_physics.h"
#include "sa/core/components/weapon_controls.h"
#include "sa/gfx/components/camera.h"
#include "sa/gfx/components/render_context.h"

namespace sa::gfx::systems {
template <typename ECST>
class ProcessEvents final : public ecs::ISystem {
 public:
  ProcessEvents(ECST &ecs)
      : m_ecs(ecs),
        m_playerTracker(ecs),
        m_cameraTracker(ecs),
        m_contextTracker(ecs) {}
  void update() final {
    // TODO: All of this needs a major cleanup later
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          std::cout << "QUIT\n";
          exit(1);
          break;
        case SDL_WINDOWEVENT:
          onWindowEvent(event.window);
          break;
        case SDL_MOUSEWHEEL:
          onMouseWheelEvent(event.wheel);
          break;
        case SDL_MOUSEBUTTONDOWN:
          onMouseButtonDown(event.button);
          break;
        case SDL_MOUSEBUTTONUP:
          onMouseButtonUp(event.button);
          break;
        case SDL_MOUSEMOTION:
          onMouseMotion(event.motion);
          break;
      }
    }

    // Process keyboard
    processKeyboard();
  }

 private:
  ECST &m_ecs;
  ecs::EntityTracker<ECST, core::component::PlayerControl,
                     core::component::SpaceshipPhysics,
                     core::component::WeaponControls>
      m_playerTracker;
  ecs::SingleEntityTracker<ECST, gfx::component::Camera> m_cameraTracker;
  ecs::SingleEntityTracker<ECST, gfx::component::RenderContext>
      m_contextTracker;

  void onWindowEvent(const SDL_WindowEvent &event) {
    switch (event.event) {
      case SDL_WINDOWEVENT_SIZE_CHANGED:
        resize(event.data1, event.data2);
        break;
    }
  }
  void onMouseWheelEvent(const SDL_MouseWheelEvent &event) {
    const float factor = 1.1f;
    if (event.y < 0) {
      zoom(factor);
    } else if (event.y > 0) {
      zoom(1.0f / factor);
    }
  }
  void onMouseButtonDown(const SDL_MouseButtonEvent &ev) {
    if (ev.button == SDL_BUTTON_RIGHT) {
      m_cameraTracker.foreach (
          [&ev](ecs::EntityId, gfx::component::Camera &camera) {
            camera.m_dragBegin.x = ev.x;
            camera.m_dragBegin.y = ev.y;
            camera.m_dragged = true;
          });
    }
  }
  void onMouseButtonUp(const SDL_MouseButtonEvent &ev) {
    if (ev.button == SDL_BUTTON_RIGHT) {
      m_cameraTracker.foreach (
          [&](ecs::EntityId, gfx::component::Camera &camera) {
            camera.m_dragged = false;
          });
    }
  }
  void onMouseMotion(const SDL_MouseMotionEvent &ev) {
    auto &camera = m_ecs.template getComponent<gfx::component::Camera>(
        m_cameraTracker.track());
    if (!camera.m_dragged) {
      return;
    }

    auto &context = m_ecs.template getComponent<gfx::component::RenderContext>(
        m_contextTracker.track());
    const glm::ivec2 winSize = sdl::size(context.m_pWindow);

    const glm::vec3 diff(float(ev.x - camera.m_dragBegin.x) / winSize.x,
                         float(ev.y - camera.m_dragBegin.y) / winSize.y, 0);

    const float pi = 3.141592f;
    const float roll = 0.0f;
    const float pitch = std::max(std::min(-5 * diff.y, pi / 2), -pi / 8);
    const float yaw = 8 * diff.x;

    // Documentation saids pitch-roll-yaw but that seems false
    camera.m_trackball = glm::normalize(glm::quat(glm::vec3(roll, pitch, yaw)));
  }

  void processKeyboard() {
    const auto *keyStates = SDL_GetKeyboardState(nullptr);

    // Spaceship controls
    if (keyStates[SDL_SCANCODE_A] && !keyStates[SDL_SCANCODE_D]) {
      control(core::component::SpaceshipPhysics::TurnCommand::left);
    } else if (!keyStates[SDL_SCANCODE_A] && keyStates[SDL_SCANCODE_D]) {
      control(core::component::SpaceshipPhysics::TurnCommand::right);
    } else {
      control(core::component::SpaceshipPhysics::TurnCommand::none);
    }

    // Weapon system controls
    control(keyStates[SDL_SCANCODE_SPACE]
                ? core::component::WeaponControls::Command::fire
                : core::component::WeaponControls::Command::idle);
  }
  void resize(int w, int h) {
    // Resize required textures
    const auto contextId = m_contextTracker.track();
    auto &context =
        m_ecs.template getComponent<component::RenderContext>(contextId);

    // Lighting
    glBindFramebuffer(GL_FRAMEBUFFER, context.m_lightingFrameBuffer.handle());
    glBindTexture(GL_TEXTURE_2D, context.m_lightingOutputColorBuffer.handle());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_FLOAT,
                 NULL);
    glBindTexture(GL_TEXTURE_2D,
                  context.m_brightLightingOutputColorBuffer.handle());
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_FLOAT,
                 NULL);
    glBindRenderbuffer(GL_RENDERBUFFER, context.m_lightingDepthBuffer.handle());
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, w, h);

    // Bloom buffers
    for (size_t n = 0; n < context.m_blurFrameBuffers.size(); n++) {
      glBindFramebuffer(GL_FRAMEBUFFER, context.m_blurFrameBuffers[n].handle());
      glBindTexture(GL_TEXTURE_2D, context.m_blurColorBuffers[n].handle());
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, w, h, 0, GL_RGBA, GL_FLOAT,
                   NULL);
    }
  }
  void zoom(float factor) {
    auto &camera = m_ecs.template getComponent<gfx::component::Camera>(
        m_cameraTracker.track());
    camera.m_distance =
        std::max(std::min(camera.m_distance * factor, 100.0f), 10.0f);
  }
  void control(core::component::SpaceshipPhysics::TurnCommand cmd) {
    m_playerTracker.foreach (
        [=](ecs::EntityId, core::component::PlayerControl &,
            core::component::SpaceshipPhysics &physics,
            core::component::WeaponControls &) { physics.m_cmd = cmd; });
  }
  void control(core::component::WeaponControls::Command cmd) {
    m_playerTracker.foreach ([=](ecs::EntityId,
                                 core::component::PlayerControl &,
                                 core::component::SpaceshipPhysics &,
                                 core::component::WeaponControls &controls) {
      controls.m_cmd = cmd;
    });
  }
};
}  // namespace sa::gfx::systems
