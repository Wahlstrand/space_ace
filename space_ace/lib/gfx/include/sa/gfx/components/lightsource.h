#pragma once

#include <glad/glad.h>

namespace sa::gfx::component {
struct Lightsource {
  glm::vec3 m_color;
  float m_power;
};
}  // namespace sa::gfx::component