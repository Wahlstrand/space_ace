#pragma once

#include <SDL.h>
#include <glad/glad.h>
#include "sa/gfx/util/gl/frame_buffer.h"
#include "sa/gfx/util/gl/mesh.h"
#include "sa/gfx/util/gl/render_buffer.h"
#include "sa/gfx/util/gl/shader_program.h"
#include "sa/gfx/util/gl/texture.h"

namespace sa::gfx::component {
struct RenderContext {
  // Window
  SDL_Window *m_pWindow;

  // Light rendering
  gl::FrameBuffer m_lightingFrameBuffer;
  gl::Texture m_lightingOutputColorBuffer;        // Floating point buffer
  gl::Texture m_brightLightingOutputColorBuffer;  // Floating point buffer
  gl::RenderBuffer m_lightingDepthBuffer;
  gl::ShaderProgram m_lightingShader;

  // Bloom
  GLuint m_nBlurPasses;
  // 0 = horizontal pass output
  // 1 = vertical pass output
  std::array<gl::Texture, 2> m_blurColorBuffers;
  std::array<gl::FrameBuffer, 2> m_blurFrameBuffers;
  gl::ShaderProgram m_blurShader;

  // Skybox
  gl::Mesh m_skyboxMesh;
  gl::ShaderProgram m_skyboxShader;
  gl::Texture m_skyboxTex;

  // Finalization
  gl::ShaderProgram m_renderShader;
  gl::Mesh m_renderRect;  // Also used by bloom for now
};
}  // namespace sa::gfx::component