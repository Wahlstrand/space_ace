#pragma once

#include <glad/glad.h>
#include <vector>

namespace sa::gfx::component {
// The resources tied to these handles
// are not handled here
struct Geometry {
  Geometry(GLuint vao, GLuint nEbo) : m_vao(vao), m_nEbo(nEbo) {}
  GLuint m_vao;
  GLuint m_nEbo;
};
struct Model {
  Model(const std::vector<Geometry> &geometry, GLuint diffuse, GLuint emissive)
      : m_geometry(geometry), m_diffuse(diffuse), m_emissive(emissive) {}
  std::vector<Geometry> m_geometry;  // TODO: Make more memory coherent?
  GLuint m_diffuse;
  GLuint m_emissive;
};
}  // namespace sa::gfx::component