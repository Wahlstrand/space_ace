#pragma once

#include <glad/glad.h>

#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

namespace sa::gfx::component {
struct Camera {
  // Lens config
  float m_zNear = 0.01f;
  float m_zFar = 1024.0f;
  float m_fov = glm::radians(45.0f);

  // Positioning
  float m_pitch = glm::radians(-5.0f);
  float m_zOffset = -2.0f;
  float m_distance = 20.0f;
  glm::quat m_trackball = glm::quat(glm::vec3(0.0, 0.0, 0.0));

  // GUI interaction. Probably move this elsewhere?
  glm::ivec2 m_dragBegin = glm::ivec2();
  bool m_dragged = false;
};
}  // namespace sa::gfx::component