#include "sa/gfx/systems/draw.h"

glm::mat4 sa::gfx::systems::projectionMatrix(float fov, float zNear, float zFar,
                                             int w, int h) {
  return glm::perspective(fov, static_cast<float>(w) / static_cast<float>(h),
                          zNear, zFar);
}
glm::mat4 sa::gfx::systems::viewMatrix(const glm::vec3 &cameraPos,
                                       const glm::quat &cameraRot) {
  const glm::vec3 upBody{0.0f, 0.0f, -1.0f};
  const glm::vec3 fwdBody{1.0f, 0.0f, 0.0f};
  const auto upWorld = cameraRot * upBody;
  const auto fwdWorld = cameraRot * fwdBody;

  return glm::lookAt(cameraPos, cameraPos + fwdWorld, upWorld);
}