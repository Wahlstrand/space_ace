#include "sa/gfx/util/assimp/mesh.h"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <sstream>

#include "assimp/Importer.hpp"
#include "assimp/mesh.h"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

namespace sa::gfx::assimp {
namespace {
struct VertexData {
  glm::vec3 m_position;
  glm::vec3 m_normal;
  glm::vec2 m_texCoords;
  // glm::vec3 m_tagent;     //These are available from assimp
  // glm::vec3 m_bitangent;  //These are available from assimp
};

gl::Mesh toMesh(const std::vector<VertexData> &vertices,
                const std::vector<GLuint> &indices) {
  GLuint vao = 0;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  GLuint vbo = 0;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(VertexData) * vertices.size(),
               vertices.data(), GL_STATIC_DRAW);

  GLuint ebo = 0;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * indices.size(),
               indices.data(), GL_STATIC_DRAW);

  glVertexAttribPointer(
      0,                                           // location of pos in the VS
      decltype(VertexData::m_position)::length(),  // n elements in this object
      GL_FLOAT,                                    // data type
      GL_FALSE,                                    // normalized
      sizeof(VertexData),                          // stride
      (void *)offsetof(VertexData, m_position));   // offset

  glVertexAttribPointer(
      1,                                         // location of pos in the VS
      decltype(VertexData::m_normal)::length(),  // n elements in this object
      GL_FLOAT,                                  // data type
      GL_FALSE,                                  // normalized
      sizeof(VertexData),                        // stride
      (void *)offsetof(VertexData, m_normal));   // offset

  glVertexAttribPointer(
      2,                                            // location of pos in the VS
      decltype(VertexData::m_texCoords)::length(),  // n elements in this object
      GL_FLOAT,                                     // data type
      GL_FALSE,                                     // normalized
      sizeof(VertexData),                           // stride
      (void *)offsetof(VertexData, m_texCoords));   // offset

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);

  return gl::Mesh(vao, vbo, ebo, static_cast<GLuint>(indices.size()));
}
std::vector<VertexData> vertices(const aiMesh *pMesh) {
  std::vector<VertexData> res;
  for (size_t n = 0; n < pMesh->mNumVertices; n++) {
    // ASSUMPTION: Always exactly one texture coordinate here! This is not true
    // for all models; they may have 0 or up to 8 coordinates. These need to be
    // handled at some point. I'm just gonna let it crash on mismatch for now;
    // resolve this later.
    const aiVector3D &aiVertex = pMesh->mVertices[n];
    const aiVector3D &aiNormal = pMesh->mNormals[n];
    const aiVector3D &aiTexture = pMesh->mTextureCoords[0][n];
    res.push_back({{aiVertex.x, aiVertex.y, aiVertex.z},
                   {aiNormal.x, aiNormal.y, aiNormal.z},
                   {aiTexture.x, aiTexture.y}});
  }
  return res;
}
std::vector<GLuint> indices(const aiMesh *pMesh) {
  std::vector<GLuint> res;
  for (size_t n = 0; n < pMesh->mNumFaces; n++) {
    const aiFace &face = pMesh->mFaces[n];
    if (face.mNumIndices != 3) {
      // We're expecting everything to be triangles right now...
      std::stringstream ss;
      ss << "Unhandled number of indices: " << face.mNumIndices;
      throw std::runtime_error(ss.str());
    }
    for (size_t i = 0; i < face.mNumIndices; i++) {
      res.push_back(face.mIndices[i]);
    }
  }
  return res;
}
std::vector<VertexData> center(std::vector<VertexData> &&vertices) {
  struct MinMaxSearch {
    void operator()(VertexData const &data) {
      m_xmin = std::min(data.m_position.x, m_xmin);
      m_ymin = std::min(data.m_position.y, m_ymin);
      m_zmin = std::min(data.m_position.z, m_zmin);
      m_xmax = std::max(data.m_position.x, m_xmax);
      m_ymax = std::max(data.m_position.y, m_ymax);
      m_zmax = std::max(data.m_position.z, m_zmax);
    }
    float m_xmin = std::numeric_limits<float>::max();
    float m_ymin = std::numeric_limits<float>::max();
    float m_zmin = std::numeric_limits<float>::max();
    float m_xmax = -std::numeric_limits<float>::max();
    float m_ymax = -std::numeric_limits<float>::max();
    float m_zmax = -std::numeric_limits<float>::max();
  };

  auto res = std::for_each(vertices.begin(), vertices.end(), MinMaxSearch());
  const float xmid = 0.5f * (res.m_xmin + res.m_xmax);
  const float ymid = 0.5f * (res.m_ymin + res.m_ymax);
  const float zmid = 0.5f * (res.m_zmin + res.m_zmax);

  struct Translator {
    void operator()(VertexData &data) {
      data.m_position.x += m_x;
      data.m_position.y += m_y;
      data.m_position.z += m_z;
    }
    float m_x;
    float m_y;
    float m_z;
  };
  std::for_each(vertices.begin(), vertices.end(),
                Translator{-xmid, -ymid, -zmid});
  return std::move(vertices);
}
std::vector<VertexData> rescale(std::vector<VertexData> &&vertices,
                                float scale) {
  std::for_each(vertices.begin(), vertices.end(), [scale](VertexData &data) {
    data.m_position.x *= scale;
    data.m_position.y *= scale;
    data.m_position.z *= scale;
  });
  return std::move(vertices);
}
gl::Mesh load(const aiMesh *pMesh, bool recenter, float scale) {
  // TODO: This is a bit inefficient as rescale is always applied even when
  // scale is 1.0.
  //      Fix when needed.
  return recenter
             ? toMesh(rescale(center(vertices(pMesh)), scale), indices(pMesh))
             : toMesh(rescale(vertices(pMesh), scale), indices(pMesh));
}

std::vector<gl::Mesh> load(aiMesh const *const *ppMeshes,
                           unsigned int numMeshes, bool recenter, float scale) {
  std::vector<gl::Mesh> res;
  for (unsigned int n = 0; n < numMeshes; n++) {
    res.push_back(load(ppMeshes[n], recenter, scale));
  }
  return res;
}
}  // namespace

std::vector<gl::Mesh> load(bool center, float scale, const std::string &path) {
  Assimp::Importer importer;
  const aiScene *pScene = importer.ReadFile(
      path, aiProcess_Triangulate |  // All primitives should be triangles
                                     // aiProcess_FlipUVs |      //Flips the
                                     // textures around y, not sure if required
                aiProcess_GenNormals |      // Create normals if not present
                aiProcess_OptimizeMeshes);  // Combine small meshes if possible
  if (!pScene) {
    std::stringstream ss;
    ss << "Failed to import file: " << path;
    throw std::runtime_error(ss.str());
  }

  return load(pScene->mMeshes, pScene->mNumMeshes, center, scale);
}
}  // namespace sa::gfx::assimp