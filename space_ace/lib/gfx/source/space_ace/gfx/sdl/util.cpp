#include "sa/gfx/util/sdl/util.h"

#include <glad/glad.h>
#include <sstream>

namespace sa::gfx::sdl {
void init() {
  if (SDL_Init(SDL_INIT_EVENTS | SDL_INIT_VIDEO) < 0) {
    std::stringstream ss;
    ss << "SDL failed to initialize: " << SDL_GetError() << std::endl;
    throw std::runtime_error(ss.str());
  }
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
}
SDLWindowUP createWindow(int w, int h) {
  SDLWindowUP upWindow(
      SDL_CreateWindow("Space Ace", 0, 0, w, h,
                       SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE),
      SDL_DestroyWindow);

  if (!upWindow) {
    std::stringstream ss;
    ss << "SDL failed to create window:: " << SDL_GetError() << std::endl;
    throw std::runtime_error(ss.str());
  }

  return upWindow;
}
SDLGLContextUP createContext(SDL_Window* pWindow) {
  SDLGLContextUP upContext(SDL_GL_CreateContext(pWindow), SDL_GL_DeleteContext);
  return upContext;
}
glm::ivec2 size(SDL_Window* pWindow) {
  int w = 0;
  int h = 0;
  SDL_GetWindowSize(pWindow, &w, &h);
  return glm::ivec2(w, h);
}
}  // namespace sa::gfx::sdl