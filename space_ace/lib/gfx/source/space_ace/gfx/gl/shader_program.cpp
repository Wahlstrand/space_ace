#include "sa/gfx/util/gl/shader_program.h"

#include <fstream>
#include <sstream>
#include <stdexcept>

namespace sa::gfx::gl {
namespace {
GLuint createShader(const std::string &str, GLenum type) {
  GLuint shader = glCreateShader(type);
  char const *cstr = str.c_str();
  glShaderSource(shader, 1, &cstr, nullptr);
  glCompileShader(shader);

  GLint shaderOk;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderOk);
  if (!shaderOk) {
    GLchar buffer[256];
    glGetShaderInfoLog(shader, 256, NULL, buffer);
    glDeleteShader(shader);
    throw std::runtime_error(buffer);
  }
  return shader;
}

std::string contents(const std::string &file) {
  std::fstream fs(file);
  if (!fs.is_open()) {
    throw std::runtime_error("Failed to open file");
  }
  std::stringstream ss;
  ss << fs.rdbuf();
  return ss.str();
}

}  // namespace
ShaderProgram ShaderProgram::load(const std::string &vsFile,
                                  const std::string &fsFile) {
  GLuint vs = 0;
  GLuint fs = 0;

  try {
    vs = createShader(contents(vsFile), GL_VERTEX_SHADER);
    fs = createShader(contents(fsFile), GL_FRAGMENT_SHADER);
  } catch (const std::exception &) {
    if (vs) {
      glDeleteShader(vs);
    }
    if (fs) {
      glDeleteShader(fs);
    }
    throw;
  }

  ShaderProgram program;
  glAttachShader(program.handle(), vs);
  glAttachShader(program.handle(), fs);
  glLinkProgram(program.handle());
  glDeleteShader(vs);  // Can be deleted now that they are linked
  glDeleteShader(fs);  // Can be deleted now that they are linked

  GLint programOk;
  glGetProgramiv(program.handle(), GL_LINK_STATUS, &programOk);
  if (!programOk) {
    GLchar buffer[256];
    glGetProgramInfoLog(program.handle(), 256, NULL, buffer);
    throw std::runtime_error(buffer);
  }

  return program;
}
ShaderProgram::ShaderProgram() : m_handle(glCreateProgram()) {}
ShaderProgram::ShaderProgram(ShaderProgram &&other) : m_handle(other.m_handle) {
  other.m_handle = 0;
}
ShaderProgram::~ShaderProgram() { glDeleteProgram(m_handle); }
ShaderProgram &ShaderProgram::operator=(ShaderProgram &&other) {
  GLuint newHandle = other.m_handle;
  other.m_handle = m_handle;
  m_handle = newHandle;
  return *this;
}
GLuint ShaderProgram::handle() const { return m_handle; }

}  // namespace sa::gfx::gl
