#include "sa/gfx/util/gl/render_buffer.h"

#include <stdexcept>

namespace sa::gfx::gl {
RenderBuffer::RenderBuffer() : m_handle(0) {
  glGenRenderbuffers(1, &m_handle);
  if (m_handle == 0) {
    throw std::runtime_error("Failed to generate render buffer");
  }
}
RenderBuffer::RenderBuffer(GLuint handle) : m_handle(handle) {}
RenderBuffer::RenderBuffer(RenderBuffer &&other) : m_handle(other.m_handle) {
  other.m_handle = 0;
}
RenderBuffer::~RenderBuffer() { glDeleteRenderbuffers(1, &m_handle); }
RenderBuffer &RenderBuffer::operator=(RenderBuffer &&other) {
  GLuint newHandle = other.m_handle;
  other.m_handle = m_handle;
  m_handle = newHandle;
  return *this;
}
GLuint RenderBuffer::handle() const { return m_handle; }

}  // namespace sa::gfx::gl