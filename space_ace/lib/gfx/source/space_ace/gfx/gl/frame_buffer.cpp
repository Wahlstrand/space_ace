#include "sa/gfx/util/gl/frame_buffer.h"

#include <stdexcept>

namespace sa::gfx::gl {
FrameBuffer::FrameBuffer() : m_handle(0) { glGenFramebuffers(1, &m_handle); }
FrameBuffer::FrameBuffer(GLuint handle) : m_handle(handle) {}
FrameBuffer::FrameBuffer(FrameBuffer &&other) : m_handle(other.m_handle) {
  other.m_handle = 0;
}
FrameBuffer::~FrameBuffer() { glDeleteFramebuffers(1, &m_handle); }

FrameBuffer &FrameBuffer::operator=(FrameBuffer &&other) {
  GLuint newHandle = other.m_handle;
  other.m_handle = m_handle;
  m_handle = newHandle;
  return *this;
}

GLuint FrameBuffer::handle() const { return m_handle; }
}  // namespace sa::gfx::gl