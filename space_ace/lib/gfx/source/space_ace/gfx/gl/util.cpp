#include "sa/gfx/util/gl/util.h"

#include <SDL.h>
#include <glm/gtc/type_ptr.hpp>
#include <sstream>

namespace sa::gfx::gl {
void init(GLuint w, GLuint h) {
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glViewport(0, 0, w, h);
}
void initGlad() {
  if (!gladLoadGLLoader(static_cast<GLADloadproc>(SDL_GL_GetProcAddress))) {
    std::stringstream ss;
    ss << "Failed to initialize gl context" << std::endl;
    throw std::runtime_error(ss.str());
  }
}

void setInt(GLuint program, const std::string &uniform, GLint val) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of int uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform1i(loc, val);
}
void setFloat(GLuint program, const std::string &uniform, float val) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of float uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform1f(loc, val);
}
void setFloats(GLuint program, const std::string &uniform, const float *pVals,
               GLuint nVals) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of float uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform1fv(loc, nVals, pVals);
}
void setVec3(GLuint program, const std::string &uniform, const glm::vec3 &val) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of vec3 uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform3f(loc, val.x, val.y, val.z);
}
void setVec3s(GLuint program, const std::string &uniform,
              const glm::vec3 *pVals, GLuint nVals) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of vec3 uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform3fv(loc, 3 * nVals,
               glm::value_ptr(pVals[0]));  // Not sure if this is truly legal
                                           // but let's do it for now.
}
void setVec4(GLuint program, const std::string &uniform, const glm::vec4 &val) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of vec4 uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniform4f(loc, val.x, val.y, val.z, val.w);
}
void setMat4(GLuint program, const std::string &uniform, const glm::mat4 &val) {
  GLint loc = glGetUniformLocation(program, uniform.c_str());
  if (loc < 0) {
    std::stringstream ss;
    ss << "Failed to get location of fmat4 uniform '" << uniform << '\'';
    throw std::runtime_error(ss.str());
  }
  glUniformMatrix4fv(loc, 1, GL_FALSE, &val[0][0]);
}
}  // namespace sa::gfx::gl