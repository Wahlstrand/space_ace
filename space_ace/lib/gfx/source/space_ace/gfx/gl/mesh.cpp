#include "sa/gfx/util/gl/mesh.h"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <vector>

namespace sa::gfx::gl {
Mesh::Mesh(GLuint vao, GLuint vbo, GLuint ebo, GLuint nEbo)
    : m_vao(vao), m_vbo(vbo), m_ebo(ebo), m_nEbo(nEbo) {}
Mesh::Mesh(Mesh &&other)
    : m_vao(other.m_vao),
      m_vbo(other.m_vbo),
      m_ebo(other.m_ebo),
      m_nEbo(other.m_nEbo) {
  other.m_vao = 0;
  other.m_vbo = 0;
  other.m_ebo = 0;
  other.m_nEbo = 0;
}
Mesh::~Mesh() {
  glDeleteVertexArrays(1, &m_vao);
  glDeleteBuffers(1, &m_vbo);
  glDeleteBuffers(1, &m_ebo);
}

Mesh &Mesh::operator=(Mesh &&other) {
  GLuint newVao = other.m_vao;
  GLuint newVbo = other.m_vbo;
  GLuint newEbo = other.m_ebo;
  GLuint newNEbo = other.m_nEbo;

  other.m_vao = m_vao;
  other.m_vbo = m_vbo;
  other.m_ebo = m_ebo;
  other.m_nEbo = m_nEbo;

  m_vao = newVao;
  m_vbo = newVbo;
  m_ebo = newEbo;
  m_nEbo = newNEbo;

  return *this;
}

GLuint Mesh::vao() const { return m_vao; }
GLuint Mesh::nEbo() const { return m_nEbo; }

}  // namespace sa::gfx::gl