#include "sa/gfx/util/gl/texture.h"

#include <iostream>
#include <sstream>
#include "sa/gfx/util/gl/image.h"

namespace sa::gfx::gl {
Texture::Texture() : m_handle(0) {
  glGenTextures(1, &m_handle);
  if (m_handle == 0) {
    throw std::runtime_error("Failed to generate texture");
  }
}
Texture::Texture(GLuint id) : m_handle(id) {}
Texture::Texture(Texture &&other) : m_handle(other.m_handle) {
  other.m_handle = 0;
}
Texture::~Texture() { glDeleteTextures(1, &m_handle); }
Texture &Texture::operator=(Texture &&other) {
  GLuint newHandle = other.m_handle;
  other.m_handle = m_handle;
  m_handle = newHandle;
  return *this;
}

GLuint Texture::handle() const { return m_handle; }

Texture Texture::create2D(Image const &img) {
  Texture texture;
  glBindTexture(GL_TEXTURE_2D, texture.handle());

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0,
               GL_RGBA,  // How to store the data on the gpu
               img.width(), img.height(), 0,
               img.format(),  // Format in the image
               GL_UNSIGNED_BYTE, img.data());
  glGenerateMipmap(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, 0);
  return texture;
}
namespace {
void mapFace(GLenum face, Image const &img) {
  glTexImage2D(face, 0, GL_RGBA, img.width(), img.height(), 0, img.format(),
               GL_UNSIGNED_BYTE, img.data());
}
}  // namespace
Texture Texture::createCubeMap(Image const &posX, Image const &negX,
                               Image const &posY, Image const &negY,
                               Image const &posZ, Image const &negZ) {
  Texture texture;
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture.handle());

  mapFace(GL_TEXTURE_CUBE_MAP_POSITIVE_X, posX);
  mapFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, negX);
  mapFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, posY);
  mapFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, negY);
  mapFace(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, posZ);
  mapFace(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, negZ);

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  return texture;
}
}  // namespace sa::gfx::gl