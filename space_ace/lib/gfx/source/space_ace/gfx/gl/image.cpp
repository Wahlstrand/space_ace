#include "sa/gfx/util/gl/image.h"

#include <sstream>
#include "stb/image.h"

namespace sa::gfx::gl {
Image::Image(GLint width, GLint height, GLenum format,
             std::unique_ptr<void, VoidDeleterT> &&upData)
    : m_width(width),
      m_height(height),
      m_format(format),
      m_upData(std::move(upData)) {}
Image::Image(Image &&other)
    : m_width(other.m_width),
      m_height(other.m_height),
      m_format(other.m_format),
      m_upData(std::move(other.m_upData)) {}

GLint Image::width() const { return m_width; }
GLint Image::height() const { return m_height; }
GLenum Image::format() const { return m_format; }
const void *Image::data() const { return m_upData.get(); }

Image Image::load(std::string const &path) {
  GLint width = 0;
  GLint height = 0;
  GLenum format = GL_RGBA;
  stbi_set_flip_vertically_on_load(true);
  void *pData =
      stbi_load(path.c_str(), &width, &height, nullptr, STBI_rgb_alpha);
  if (pData == nullptr) {
    std::stringstream ss;
    ss << "Failed to load image '" << path << "'";
    throw std::runtime_error(ss.str());
  }
  return Image(
      width, height, format,
      std::unique_ptr<void, Image::VoidDeleterT>(pData, stbi_image_free));
}

}  // namespace sa::gfx::gl